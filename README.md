# Core Bank Application Demo #

## Database Setup ##

1. Create database user

    ```
    grant all on corebankdb.* to training@localhost identified by 'java';
    ```

2. Create database

    ```
    create database corebankdb;
    ```

## Use the application ##

* Balance Inquiry

    ```
    curl --request GET 'localhost:8080/card/1234567800001111/balance?pin=1234'
    ```

    [![Balance Inquiry](docs/img/01-balance-inquiry.png)](docs/img/01-balance-inquiry.png)

* Cash Withdrawal

    ```
    curl --request POST 'localhost:8080/card/1234567800001111/withdrawal' \
    --form 'pin=1234' \
    --form 'amount=101' \
    --form 'clientReference=c111'
    ```

    [![Cash Withdrawal](docs/img/02-cash-withdrawal.png)](docs/img/02-cash-withdrawal.png)

* Cash Withdrawal Partial Reversal (amount reversed less than amount withdrawn)

    ```
    curl --request POST 'localhost:8080/reversal/withdrawal/c111' \
    --form 'amount=50'
    ```
  
* Cash Withdrawal Full Reversal (amount reversed equals to amount withdrawn)

    ```
    curl --request POST 'localhost:8080/reversal/withdrawal/c111' \
    --form 'amount=101'
    ```

    [![Withdrawal Reversal](docs/img/03-withdrawal-reversal.png)](docs/img/03-withdrawal-reversal.png)

* Fund Transfer (On Us)

    ```
    curl --request POST 'localhost:8080/card/1234567800001111/transfer' \
    --form 'pin=1234' \
    --form 'destination=12345678902' \
    --form 'amount=17' \
    --form 'clientReference=c222'
    ```

    [![Fund Transfer](docs/img/04-fund-transfer.png)](docs/img/04-fund-transfer.png)

* Fund Transfer (On Us) Reversal

    ```
    curl --request POST 'localhost:8080/reversal/transfer/c222'
    ```
  
    [![Fund Transfer Reversal](docs/img/05-transfer-reversal.png)](docs/img/05-transfer-reversal.png)

## Error Scenario ##

* Invalid Pin

    ```
    curl --request POST 'localhost:8080/card/1234567800001111/withdrawal' \
    --form 'pin=111' \
    --form 'amount=100' \
    --form 'clientReference=c111'
    ```

    [![Invalid Pin](docs/img/06-wrong-pin.png)](docs/img/06-wrong-pin.png)

* Insufficient Amount

    ```
    curl --request POST 'localhost:8080/card/1234567800001111/withdrawal' \
        --form 'pin=1234' \
        --form 'amount=100001' \
        --form 'clientReference=c111'
    ```
  
    [![Insufficient Amount](docs/img/07-insufficient-balance.png)](docs/img/07-insufficient-balance.png)