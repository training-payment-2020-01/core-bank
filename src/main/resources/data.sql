delete from account_transaction;
delete from card;
delete from account;
delete from customer;

insert into customer (id, customer_number, name)
values ('c001', '10001', 'Customer 001');

insert into customer (id, customer_number, name)
values ('c002', '10002', 'Customer 002');


insert into account (id, id_owner, account_number, current_balance, total_debet, total_credit)
values ('a001', 'c001', '12345678901', 100000, 0, 0);

insert into account (id, id_owner, account_number, current_balance, total_debet, total_credit)
values ('a002', 'c002', '12345678902', 200000, 0, 0);

insert into card (id, id_account, card_number, pin)
values ('card001', 'a001', '1234567800001111', '1234');

insert into card (id, id_account, card_number, pin)
values ('card002', 'a002', '1234567800002222', '1234');
