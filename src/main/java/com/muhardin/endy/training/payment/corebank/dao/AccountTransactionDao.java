package com.muhardin.endy.training.payment.corebank.dao;

import com.muhardin.endy.training.payment.corebank.entity.AccountTransaction;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AccountTransactionDao extends PagingAndSortingRepository<AccountTransaction, String> {
    List<AccountTransaction> findByClientReference(String clientReference);
}
