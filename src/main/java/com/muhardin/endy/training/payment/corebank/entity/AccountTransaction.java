package com.muhardin.endy.training.payment.corebank.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity @Data
public class AccountTransaction {
    @Id @Column(length = 36)
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_account")
    private Account account;

    @NotNull
    private LocalDateTime transactionTime = LocalDateTime.now();

    @NotNull
    private BigDecimal amount = BigDecimal.ZERO;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @NotNull @NotEmpty
    private String clientReference;

    @NotNull @NotEmpty
    private String serverReference;
}
