package com.muhardin.endy.training.payment.corebank.exception;

import lombok.Getter;

public class BankingException extends Exception {
    @Getter
    private String errorCode = "99";

    public BankingException() {
        super();
    }

    public BankingException(String message) {
        super(message);
    }
}
