package com.muhardin.endy.training.payment.corebank.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data @Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"accountNumber"}))
public class Account {

    @Id @Column(length = 36)
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotNull
    @NotEmpty
    private String accountNumber;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_owner")
    private Customer owner;

    @NotNull
    private BigDecimal currentBalance = BigDecimal.ZERO;
    @NotNull
    private BigDecimal totalDebet = BigDecimal.ZERO;
    @NotNull
    private BigDecimal totalCredit = BigDecimal.ZERO;
}
