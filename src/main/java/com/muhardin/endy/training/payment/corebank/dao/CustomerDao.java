package com.muhardin.endy.training.payment.corebank.dao;

import com.muhardin.endy.training.payment.corebank.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
}
