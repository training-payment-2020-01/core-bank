package com.muhardin.endy.training.payment.corebank.exception;

import lombok.Getter;

public class InvalidAccountException extends BankingException {
    @Getter
    private String errorCode = "12";

    public InvalidAccountException() {
        super();
    }

    public InvalidAccountException(String message) {
        super(message);
    }
}
