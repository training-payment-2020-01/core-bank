package com.muhardin.endy.training.payment.corebank.entity;

public enum TransactionType {
    CASH_WITHDRAWAL,
    TRANSFER_IN,
    TRANSFER_OUT,
    REVERSE_CASH_WITHDRAWAL_PARTIAL,
    REVERSE_CASH_WITHDRAWAL_FULL,
    REVERSE_TRANSFER_IN,
    REVERSE_TRANSFER_OUT
}
