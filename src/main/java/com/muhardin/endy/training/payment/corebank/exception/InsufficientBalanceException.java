package com.muhardin.endy.training.payment.corebank.exception;

import lombok.Getter;

public class InsufficientBalanceException extends BankingException {
    @Getter
    private String errorCode = "21";

    public InsufficientBalanceException() {
        super();
    }

    public InsufficientBalanceException(String message) {
        super(message);
    }
}
