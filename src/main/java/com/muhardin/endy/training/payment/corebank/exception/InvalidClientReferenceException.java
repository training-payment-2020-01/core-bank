package com.muhardin.endy.training.payment.corebank.exception;

import lombok.Getter;

public class InvalidClientReferenceException extends BankingException {
    @Getter
    private String errorCode = "30";
    public InvalidClientReferenceException() {
        super();
    }

    public InvalidClientReferenceException(String message) {
        super(message);
    }
}
