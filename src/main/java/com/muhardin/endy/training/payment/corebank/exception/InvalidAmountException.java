package com.muhardin.endy.training.payment.corebank.exception;

import lombok.Getter;

public class InvalidAmountException extends BankingException {
    @Getter
    private String errorCode = "20";

    public InvalidAmountException() {
        super();
    }

    public InvalidAmountException(String message) {
        super(message);
    }
}
