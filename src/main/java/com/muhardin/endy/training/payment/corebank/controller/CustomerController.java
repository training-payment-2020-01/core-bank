package com.muhardin.endy.training.payment.corebank.controller;

import com.muhardin.endy.training.payment.corebank.dao.AccountDao;
import com.muhardin.endy.training.payment.corebank.dao.AccountTransactionDao;
import com.muhardin.endy.training.payment.corebank.dao.CustomerDao;
import com.muhardin.endy.training.payment.corebank.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired private CustomerDao customerDao;
    @Autowired private AccountDao accountDao;
    @Autowired private AccountTransactionDao accountTransactionDao;

    @GetMapping("/")
    public Page<Customer> findCustomer(Pageable pageable) {
        return customerDao.findAll(pageable);
    }
}
