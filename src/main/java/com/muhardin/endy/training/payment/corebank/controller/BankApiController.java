package com.muhardin.endy.training.payment.corebank.controller;

import com.muhardin.endy.training.payment.corebank.exception.*;
import com.muhardin.endy.training.payment.corebank.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class BankApiController {
    @Autowired private BankService bankService;

    @GetMapping("/card/{number}/balance")
    public ResponseEntity<Map<String, Object>> balanceInquiry(@PathVariable String number, @RequestParam String pin) {
        Map<String, Object> result = new HashMap<>();
        try {
            result.put("amount", bankService.inquireBalance(number, pin));
            result.put("response_code", "00");
            return ResponseEntity.ok(result);
        } catch (BankingException e) {
            result.put("response_code", e.getErrorCode());
            result.put("error", e.getMessage());
            return ResponseEntity.badRequest().body(result);
        }

    }

    @PostMapping("/card/{number}/withdrawal")
    public ResponseEntity<Map<String, Object>> cashWithdrawal(@PathVariable String number, @RequestParam String pin,
                                                 @RequestParam BigDecimal amount, @RequestParam String clientReference) {
        Map<String, Object> result = new HashMap<>();
            result.put("response_code", "00");
        try {
            result.put("reference", bankService.withdrawCash(number, pin, amount, clientReference));
        } catch (BankingException e) {
            result.put("response_code", e.getErrorCode());
            result.put("error", e.getMessage());
            return ResponseEntity.badRequest().body(result);
        }
        return ResponseEntity.ok(result);
    }

    @PostMapping("/reversal/withdrawal/{clientReference}")
    public ResponseEntity<Map<String, Object>> cashWithdrawalReversal(@RequestParam BigDecimal amount,
                                                         @PathVariable String clientReference) {
        Map<String, Object> result = new HashMap<>();
        result.put("response_code", "00");

        try {
            result.put("reference", bankService.reverseCashWithdrawal(clientReference, amount));
            return ResponseEntity.ok(result);
        } catch (BankingException e) {
            result.put("response_code", e.getErrorCode());
            result.put("error", e.getMessage());
            return ResponseEntity.badRequest().body(result);
        }
    }

    @PostMapping("/card/{number}/transfer")
    public ResponseEntity<Map<String, Object>> transfer(@PathVariable String number, @RequestParam String pin,
                                                 @RequestParam String destination,
                                                 @RequestParam BigDecimal amount,
                                           @RequestParam String clientReference) {
        Map<String, Object> result = new HashMap<>();
        result.put("response_code", "00");
        try {
            result.put("reference", bankService.transferOnUs(number, pin, destination, amount, clientReference));
            return ResponseEntity.ok(result);
        } catch (BankingException e) {
            result.put("response_code", e.getErrorCode());
            result.put("error", e.getMessage());
            return ResponseEntity.badRequest().body(result);
        }
    }

    @PostMapping("/reversal/transfer/{clientReference}")
    public ResponseEntity<Map<String, Object>> transferReversal(@PathVariable String clientReference) {
        Map<String, Object> result = new HashMap<>();
        result.put("response_code", "00");
        try {
            result.put("reference", bankService.reverseTransferOnUs(clientReference));
            return ResponseEntity.ok(result);
        } catch (BankingException e) {
            result.put("response_code", e.getErrorCode());
            result.put("error", e.getMessage());
            return ResponseEntity.badRequest().body(result);
        }
    }
}
