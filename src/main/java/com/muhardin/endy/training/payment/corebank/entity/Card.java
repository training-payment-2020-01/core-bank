package com.muhardin.endy.training.payment.corebank.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity @Data
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"cardNumber"}))
public class Card {
    @Id
    @Column(length = 36)
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty @Size(min = 16, max = 16)
    private String cardNumber;

    @NotNull @NotEmpty
    private String pin;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_account")
    private Account account;

}
