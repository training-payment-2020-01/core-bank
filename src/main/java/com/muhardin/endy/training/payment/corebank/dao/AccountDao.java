package com.muhardin.endy.training.payment.corebank.dao;

import com.muhardin.endy.training.payment.corebank.entity.Account;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface AccountDao extends PagingAndSortingRepository<Account, String> {
    Optional<Account> findByAccountNumber(String destinationAccountNumber);
}
