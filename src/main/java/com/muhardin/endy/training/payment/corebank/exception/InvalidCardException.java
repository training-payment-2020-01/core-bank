package com.muhardin.endy.training.payment.corebank.exception;

import lombok.Getter;

public class InvalidCardException extends BankingException {
    @Getter
    private String errorCode = "10";
    public InvalidCardException() {
        super();
    }

    public InvalidCardException(String message) {
        super(message);
    }
}
