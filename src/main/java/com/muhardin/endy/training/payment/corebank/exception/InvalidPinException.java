package com.muhardin.endy.training.payment.corebank.exception;

import lombok.Getter;

public class InvalidPinException extends BankingException {
    @Getter
    private String errorCode = "11";

    public InvalidPinException() {
        super();
    }

    public InvalidPinException(String message) {
        super(message);
    }
}
