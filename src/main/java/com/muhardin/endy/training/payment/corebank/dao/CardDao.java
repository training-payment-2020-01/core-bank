package com.muhardin.endy.training.payment.corebank.dao;

import com.muhardin.endy.training.payment.corebank.entity.Card;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CardDao extends PagingAndSortingRepository<Card, String> {
    Optional<Card> findByCardNumber(String cardNumber);
}
