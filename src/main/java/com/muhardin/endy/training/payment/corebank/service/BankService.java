package com.muhardin.endy.training.payment.corebank.service;

import com.muhardin.endy.training.payment.corebank.dao.AccountDao;
import com.muhardin.endy.training.payment.corebank.dao.AccountTransactionDao;
import com.muhardin.endy.training.payment.corebank.dao.CardDao;
import com.muhardin.endy.training.payment.corebank.dao.CustomerDao;
import com.muhardin.endy.training.payment.corebank.entity.Account;
import com.muhardin.endy.training.payment.corebank.entity.AccountTransaction;
import com.muhardin.endy.training.payment.corebank.entity.Card;
import com.muhardin.endy.training.payment.corebank.entity.TransactionType;
import com.muhardin.endy.training.payment.corebank.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service @Transactional
public class BankService {

    @Autowired private CustomerDao customerDao;
    @Autowired private AccountDao accountDao;
    @Autowired private AccountTransactionDao accountTransactionDao;
    @Autowired private CardDao cardDao;

    public BigDecimal inquireBalance(String cardNumber, String pin)
            throws InvalidCardException,
            InvalidPinException {
        Card card = verifyAndGetCard(cardNumber);
        verifyPin(pin, card.getPin());
        return card.getAccount().getCurrentBalance();
    }

    public String withdrawCash(String cardNumber, String pin,
                             BigDecimal amount, String clientReference)
            throws InvalidCardException,
            InvalidPinException, InsufficientBalanceException, InvalidAmountException {
        Card card = verifyAndGetCard(cardNumber);
        verifyPin(pin, card.getPin());
        verifyAmount(amount, card.getAccount());
        AccountTransaction trx = saveTransaction(TransactionType.CASH_WITHDRAWAL, amount, clientReference, card.getAccount());
        updateAccountBalance(amount, card.getAccount(), TransactionType.CASH_WITHDRAWAL);
        return trx.getServerReference();
    }

    public String transferOnUs(String cardNumber, String pin, String destinationAccountNumber,
                             BigDecimal amount, String clientReference)
            throws InvalidCardException, InvalidAccountException,
            InvalidPinException, InsufficientBalanceException, InvalidAmountException {
        Card card = verifyAndGetCard(cardNumber);
        verifyPin(pin, card.getPin());
        verifyAmount(amount,card.getAccount());
        String serverReference = UUID.randomUUID().toString();

        Optional<Account> optDest = accountDao.findByAccountNumber(destinationAccountNumber);
        if (!optDest.isPresent()) {
            throw new InvalidAccountException("Account " + destinationAccountNumber + " not found");
        }

        updateAccountBalance(amount, card.getAccount(), TransactionType.TRANSFER_OUT);
        saveTransaction(TransactionType.TRANSFER_OUT, amount, serverReference, clientReference, card.getAccount());

        updateAccountBalance(amount, optDest.get(), TransactionType.TRANSFER_IN);
        saveTransaction(TransactionType.TRANSFER_IN, amount, serverReference, clientReference, optDest.get());
        return serverReference;
    }

    public String reverseCashWithdrawal(String clientReference, BigDecimal reversedAmount)
            throws InvalidClientReferenceException, InvalidAmountException {
        List<AccountTransaction> transactions = searchAccountTransactions(clientReference);
        String serverReference = UUID.randomUUID().toString();

        for (AccountTransaction at : transactions) {
            AccountTransaction reversal = new AccountTransaction();
            reversal.setAccount(at.getAccount());
            reversal.setAmount(reversedAmount);
            reversal.setServerReference(serverReference);
            reversal.setClientReference(clientReference);

            if (reversedAmount.compareTo(at.getAmount()) >= 0) {
                throw new InvalidAmountException("Reversed amount exceeds original amount");
            }

            if (reversedAmount.compareTo(at.getAmount()) == 0) {
                reversal.setTransactionType(TransactionType.REVERSE_CASH_WITHDRAWAL_FULL);
                updateAccountBalance(reversedAmount, at.getAccount(), TransactionType.REVERSE_CASH_WITHDRAWAL_FULL);
            } else {
                reversal.setTransactionType(TransactionType.REVERSE_CASH_WITHDRAWAL_PARTIAL);
                updateAccountBalance(reversedAmount, at.getAccount(), TransactionType.REVERSE_CASH_WITHDRAWAL_PARTIAL);
            }

            accountTransactionDao.save(reversal);
        }

        return serverReference;
    }

    public String reverseTransferOnUs(String clientReference) throws InvalidClientReferenceException {
        List<AccountTransaction> transactions = searchAccountTransactions(clientReference);
        String serverReference = UUID.randomUUID().toString();
        for (AccountTransaction at : transactions) {
            AccountTransaction reversal = new AccountTransaction();
            reversal.setAccount(at.getAccount());
            reversal.setAmount(at.getAmount());
            reversal.setServerReference(serverReference);
            reversal.setClientReference(clientReference);

            if (TransactionType.TRANSFER_IN.equals(at.getTransactionType())) {
                reversal.setTransactionType(TransactionType.REVERSE_TRANSFER_IN);
            }

            if (TransactionType.TRANSFER_OUT.equals(at.getTransactionType())) {
                reversal.setTransactionType(TransactionType.REVERSE_TRANSFER_OUT);
            }

            updateAccountBalance(at.getAmount(), at.getAccount(), reversal.getTransactionType());
            accountTransactionDao.save(reversal);
        }
        return serverReference;
    }

    private Card verifyAndGetCard(String cardNumber) throws InvalidCardException {
        Optional<Card> optCard = cardDao.findByCardNumber(cardNumber);

        if (!optCard.isPresent()) {
            throw new InvalidCardException("Card " + cardNumber + " not found");
        }
        return optCard.get();
    }

    private void verifyPin(String pinInput, String pinDb) throws InvalidPinException {
        if (!StringUtils.hasText(pinInput)) {
            throw new InvalidPinException("PIN is required");
        }

        if (!pinInput.equals(pinDb)) {
            throw new InvalidPinException("PIN is incorrect");
        }
    }

    private void verifyAmount(BigDecimal amount, Account account) throws InvalidAmountException, InsufficientBalanceException {
        if (amount == null || amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidAmountException("Amount is required and must be positive");
        }

        if (amount.compareTo(account.getCurrentBalance()) > 0) {
            throw new InsufficientBalanceException("Amount withdrawn exceeds current balance");
        }
    }

    private AccountTransaction saveTransaction(TransactionType type, BigDecimal amount, String clientReference, Account account) {
        return saveTransaction(type, amount, UUID.randomUUID().toString(), clientReference, account);
    }

    private AccountTransaction saveTransaction(TransactionType type, BigDecimal amount, String serverReference, String clientReference, Account account) {
        AccountTransaction at = new AccountTransaction();
        at.setTransactionType(type);
        at.setAmount(amount);
        at.setAccount(account);
        at.setClientReference(clientReference);
        at.setServerReference(serverReference);
        at.setServerReference(UUID.randomUUID().toString());
        accountTransactionDao.save(at);
        return at;
    }

    private void updateAccountBalance(BigDecimal amount, Account account, TransactionType type) {
        if(TransactionType.CASH_WITHDRAWAL.equals(type) ||
                TransactionType.TRANSFER_OUT.equals(type) ||
                TransactionType.REVERSE_TRANSFER_IN.equals(type)) {
            account.setCurrentBalance(account.getCurrentBalance().subtract(amount));
            account.setTotalDebet(account.getTotalDebet().add(amount));
        }

        if(TransactionType.REVERSE_CASH_WITHDRAWAL_FULL.equals(type) ||
                TransactionType.REVERSE_CASH_WITHDRAWAL_PARTIAL.equals(type) ||
                TransactionType.TRANSFER_IN.equals(type) ||
                TransactionType.REVERSE_TRANSFER_OUT.equals(type)) {
            account.setCurrentBalance(account.getCurrentBalance().add(amount));
            account.setTotalCredit(account.getTotalCredit().add(amount));
        }

        accountDao.save(account);
    }

    private List<AccountTransaction> searchAccountTransactions(String clientReference) throws InvalidClientReferenceException {
        List<AccountTransaction> transactions = accountTransactionDao.findByClientReference(clientReference);
        if (transactions.isEmpty()) {
            throw new InvalidClientReferenceException("Client reference " + clientReference + " not found");
        }
        return transactions;
    }
}
