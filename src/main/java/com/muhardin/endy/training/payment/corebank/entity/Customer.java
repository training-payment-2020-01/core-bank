package com.muhardin.endy.training.payment.corebank.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity @Data
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"customerNumber"})})
public class Customer {

    @Id @Column(length = 36)
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty
    private String customerNumber;

    @NotNull @NotEmpty
    private String name;
}
